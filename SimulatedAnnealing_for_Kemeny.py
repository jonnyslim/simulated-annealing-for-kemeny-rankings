import sys
import pdb
import os
import time
import numpy
import random
import math
import copy
import matplotlib.pyplot as plt

# DEFINES
INITIAL_TEMPERATURE = 50 # total MoV = 2625, initial Cost = 749
TEMPERATURE_LOOP = 500
COOLING_COEFFICIENT = 0.9
NUM_NON_IMPROVE = 10000

index_lookup = {}
tournament_matrix = []



def SimulatedAnnealing(player_ranking, matrix_ranking, initial_temperature=None, temperature_loop=None, cooling_coefficient=None, num_non_improve=None):
	T = INITIAL_TEMPERATURE
	t_l = TEMPERATURE_LOOP
	c_c = COOLING_COEFFICIENT
	n_i = NUM_NON_IMPROVE

	# Only used for ParameterTest()
	"""if initial_temperature is not None:
		T = initial_temperature
	if temperature_loop is not None:
		t_l = temperature_loop
	if cooling_coefficient is not None:
		c_c = cooling_coefficient
	if num_non_improve is not None:
		n_i = num_non_improve
	"""

	x_now = copy.deepcopy(player_ranking)
	x_best = copy.deepcopy(player_ranking)
	now_matrix = copy.deepcopy(matrix_ranking)
	best_matrix = copy.deepcopy(matrix_ranking)

	cost_now = CalculateCost(now_matrix)
	cost_best = cost_now

	iter_count = 0
	while (iter_count <= n_i):
		for i in range(0,t_l):
			iter_count+=1
			x_next, next_matrix = findNeighbour(x_now, now_matrix)
			cost_next = CalculateCost(next_matrix)
			delta_cost = cost_next - cost_now
			if delta_cost <= 0:
				x_now = copy.deepcopy(x_next)
				now_matrix = copy.deepcopy(next_matrix)
				cost_now = cost_next
			else:
				q = random.random()
				if q < math.exp(-delta_cost/T):
					x_now = copy.deepcopy(x_next)
					now_matrix = copy.deepcopy(next_matrix)
					cost_now = cost_next

			if (cost_now < cost_best):
				x_best = copy.deepcopy(x_now)
				best_matrix = copy.deepcopy(now_matrix)
				cost_best = cost_now
				iter_count = 0
		T *= c_c

	return x_best, best_matrix



def CalculateCost(R):
	new_cost = 0
	""" calculate cost """
	result = tournament_matrix * R.T
	new_cost = result.sum()

	return new_cost



def findNeighbour(in_list, in_matrix):
	global index_lookup
	out_list = copy.deepcopy(in_list)
	out_matrix = copy.deepcopy(in_matrix)

	""" Generate 2 random players to switch """
	rand1 = random.randint(1,len(out_list))
	rand2 = rand1
	while rand2 == rand1:
		rand2 = random.randint(1,len(out_list))

	""" Switch players in ranking list """
	temp = out_list[rand1]
	out_list[rand1] = out_list[rand2]
	out_list[rand2] = temp

	# depends on whether rand1 < rand2
	if rand1 > rand2:
		temp = rand1
		rand1 = rand2
		rand2 = temp

	""" keeps track of the original matrix indexes """
	index1 = index_lookup[in_list[rand1]] - 1
	index2 = index_lookup[in_list[rand2]] - 1

	""" The swapping of the rows and columns for the switched players in ranking matrix """
	out_matrix[:, (index1, index2)] = out_matrix[:, (index2, index1)]
	out_matrix[(index1, index2), :] = out_matrix[(index2, index1), :]

	return out_list, out_matrix



def readWMGFile(infile):
	with open(infile,"r") as readfile:
		""" Get # of participants """
		count = int(readfile.readline())

		""" Get participant names & initialise rank matrix"""
		global index_lookup
		participant_list = {}
		for i in range(0,count):
			line = readfile.readline()
			(key, val) = line.split(',')
			participant_list[int(key)] = val.rstrip()
			index_lookup[val.rstrip()] = int(key)

		r_matrix = numpy.triu(numpy.ones((count,count), dtype=int), 1)

		""" Get margins of victory """
		global tournament_matrix
		tournament_matrix = numpy.zeros((count,count), dtype=int)
		readfile.readline()	# skip over line
		for line in readfile:
			(margin, player, opponent) = line.split(',')
			tournament_matrix[int(player)-1][int(opponent)-1] = int(margin)

	return participant_list, r_matrix



def main():
	wmg_file = sys.argv[1]
	start = time.time()

	(participant_list, ranking_matrix) = readWMGFile(wmg_file)
	
	(Ranking_Result, Kemeny_Matrix) = SimulatedAnnealing(participant_list, ranking_matrix)

	for (rank, player) in Ranking_Result.items():
		print(rank, player)

	Kemeny_Ranking = CalculateCost(Kemeny_Matrix)
	print("\nKemeny Score:" , Kemeny_Ranking)


	end = time.time()
	deltaT = end - start
	print("\nTime Taken:", deltaT*1000)


# For testing parameters only
""" 
def Parameter_Test():
	wmg_file = sys.argv[1]

	Kemeny_List = []
	T_I = range(5,500,10)
	T_L = range(100,1000, 20)
	C_C = numpy.linspace(0.8, 0.996, 50)
	N_I = range(5000,15000, 200)

	(participant_list, ranking_matrix) = readWMGFile(wmg_file)
	
	for i in T_I:
		(Ranking_Result, Kemeny_Matrix) = SimulatedAnnealing(participant_list, ranking_matrix, initial_temperature=i)
		Kemeny_List.append(CalculateCost(Kemeny_Matrix))
		# pdb.set_trace()

	fig1, ax = plt.subplots()
	ax.plot(T_I, Kemeny_List)
	ax.set(xlabel='Initial Temperature', ylabel='Kemeny Score', 
		title='Results of Simulated Annealing with varying Initial Temperature ')
	ax.set_ylim(bottom=0)
	ax.grid(True, which='both')
	ax.minorticks_on()
	ax.grid(which='minor', color='gray', linewidth=0.15)
	fig1.savefig("Ti_Variation.png")
	# plt.show()	

	Kemeny_List = []
	for i in T_L:
		(Ranking_Result, Kemeny_Matrix) = SimulatedAnnealing(participant_list, ranking_matrix, temperature_loop=i)
		Kemeny_List.append(CalculateCost(Kemeny_Matrix))

	fig2, ax = plt.subplots()
	ax.plot(T_L, Kemeny_List)
	ax.set(xlabel='Number of Inner Loops', ylabel='Kemeny Score', 
		title='Results of Simulated Annealing with varying Inner Loop Iterations ')
	ax.set_ylim(bottom=0)
	ax.grid(True, which='both')
	ax.minorticks_on()
	ax.grid(which='minor', color='gray', linewidth=0.15)
	fig2.savefig("Tl_Variation.png")
	# plt.show()

	Kemeny_List = []
	for i in C_C:
		(Ranking_Result, Kemeny_Matrix) = SimulatedAnnealing(participant_list, ranking_matrix, cooling_coefficient=i)
		Kemeny_List.append(CalculateCost(Kemeny_Matrix))

	fig3, ax = plt.subplots()
	ax.plot(C_C, Kemeny_List)
	ax.set(xlabel='Cooling Coefficient', ylabel='Kemeny Score', 
		title='Results of Simulated Annealing with varying Cooling Coefficient')
	ax.set_ylim(bottom=0)
	ax.grid(True, which='both')
	ax.minorticks_on()
	ax.grid(which='minor', color='gray', linewidth=0.15)
	fig3.savefig("CC_Variation.png")
	# plt.show()

	Kemeny_List = []
	for i in N_I:
		(Ranking_Result, Kemeny_Matrix) = SimulatedAnnealing(participant_list, ranking_matrix, num_non_improve=i)
		Kemeny_List.append(CalculateCost(Kemeny_Matrix))

	fig4, ax = plt.subplots()
	ax.plot(N_I, Kemeny_List)
	ax.set(xlabel='Number of Iterations', ylabel='Kemeny Score', 
		title='Results of Simulated Annealing with varying Outer Loop Iterations')
	ax.set_ylim(bottom=0)
	ax.grid(True, which='both')
	ax.minorticks_on()
	ax.grid(which='minor', color='gray', linewidth=0.15)
	fig4.savefig("NI_Variation.png")
"""

if __name__ == "__main__":
	main()
	# Parameter_Test()