# Simulated Annealing for Kemeny Rankings

Simulated Annealing applied on the Kemeny Scores of F1 drivers for the 1994 championship (Python)

The Margin of Victory of Player A with respect to Player B refers to the degree to which A beat B. In this F1 example, Player A's margin of victory is the number of races he finished above Player C. E.g Player A has a MoV of 14 w.r.t B if he finished above B 14 more times. A kemeny score of 0 for A w.r.t B means that Player A either drew with B or B finished above A more times.

This project aims to find the best Kemeny Ranking of all the F1 drivers. A kemeny score is the value referring to the number of players who had better MoV's than another, but were ranked below them. This means the lower the Kemeny score, the more accurate the Kemeny Ranking. This gives us a clear optimisation problem, with which Simulated Annealing is used to solve.
